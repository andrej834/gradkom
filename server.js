var mime = require('mime-types');
var formidable = require('formidable');
var http = require('http');
var fs = require('fs-extra');
var util = require('util');
var path = require('path');
var contentDisposition = require('content-disposition');

var port = 8080;
var file_directory = "./data/";


/* Inicializacija serverja ter vseh preusmeritev */

var server = http.createServer(function(req, res) {
  var url_link = req.url;
  if (url_link == "/") {
	    console.log("homepage");
      redirect_to_homepage(res);
  } else if (url_link == "/datoteke") {
      list_files(res);
      console.log("files");
  } else if (url_link.startsWith("/delete")) {
      redirect_delete_files(res, req.url);
      console.log("delete files");
  } else if (url_link.startsWith("/download")) {
	    redirect_to_download(res, req.url);
	    console.log("download");
  } else if (url_link == "/upload") {
      upload_File(req, res);
      console.log("upload");
  } else if (url_link.startsWith("/preview")){
      redirect_to_preview(res, req.url);
	    console.log("preview");
  } else {
      redirect(res, "./public"+req.url,"unknown");
  }
});

/* Zagon serverja */

server.listen(port, function() {
  console.log('Server is up and running.');
});

/* Posreduj homepage */

function redirect_to_homepage(res) {
  redirect(res, "./homepage.html", "text/html");
}

/* Posreduj seznam datotek, ki so navoljo*/

function list_files(res) {
  res.writeHead(200, {'Content-Type': 'application/json'});
  fs.readdir(file_directory, function(err, files) {
    if (err) {
      error500(res);
    } else {
      var allFiles = [];
      for (var i=0; i < files.length; i++) {
        var file = files[i];
        var size = fs.statSync(file_directory+file).size;
        allFiles.push({Datoteka: file, Velikost: size});
      }
      res.write(JSON.stringify(allFiles));
      res.end();
    }
  });
}

/* Posreduj vsebino */

function redirect(res, absolute_path, type) {
  fs.exists(absolute_path, function(exists) {
    if (exists) {
      fs.readFile(absolute_path, function(err, data) {
        if (err) {
          error500(res);
        } else {
          send_File(res, absolute_path, data, type);
        }
      });
    } else {
      error404(res);
    }
  });
}

function send_File(res, absolute_path, data, type) {
  if (type=="unknown"){  
	  res.writeHead(200, {'Content-Type': mime.lookup(path.basename(absolute_path))});
  } else if(absolute_path.indexOf(".mp4")>-1){
    res.writeHead(200, {'Content-Type': "application/force-download"});
  } else {
  	res.writeHead(200, {'Content-Type': type});
  }
  res.end(data);
}

function redirect_to_download(res, urlRequest){
	redirect(res, file_directory+urlRequest.replace("/download",""), "application/octet-stream");
}

/* Brisanje datotek */

function redirect_delete_files(res, urlRequest){
	delete_files(res, file_directory + urlRequest.replace("/delete",""));
}

function delete_files(res, file){
  res.writeHead(200, {'Content-Type': 'text/plain'});
  fs.unlink(file, function(err){
    if (err){
      error404(res);   
    }else{
      res.end("File deleted.");      
    }
  });
}

/* Predogled datotek */

function redirect_to_preview(res, urlRequest){
  redirect(res, file_directory + urlRequest.replace("/preview", ""), "unknown");    
}

/* Nalaganje datoteke v bazo */

function upload_File(req, res){
  console.log("req: "+req.url);
  var form = new formidable.IncomingForm();
  console.log(form);
  form.parse(req, function(err, field, file){
    if(err){
      error500(res);
    }else{
     util.inspect({fields: field, files: file}); 
    }
  });
  
  form.on('end', function(fields, files){
    var temp_Path = this.openedFiles[0].path;
    console.log("tempPath: "+temp_Path);
    var file = this.openedFiles[0].name.replace(" ", "_");
    
    console.log("file: "+file);
    fs.copy(temp_Path, file_directory + file, function(err){
      if(err){
        error404(res);
      }else{
        redirect_to_homepage(res);
      }
    })
  });
}

/* Obravnavanje napak */

function error404(res){
	res.writeHead(404, {'Content-Type': 'text/html'});
    res.end('Napaka 404 : datoteke ni bilo mogoce najti.');
}

function error500(res) {
    res.writeHead(500, {'Content-Type': 'text/plain'});
    res.end('Napaka 500: napaka streznika.');
}