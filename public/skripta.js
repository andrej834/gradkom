window.addEventListener('load', function() {
	var url = window.location.href;
	
	if(url.indexOf("upload")>-1){
		window.location.href = url.replace("upload", "");
	}

	var prizgiCakanje = function() {
		document.querySelector(".loading").style.display = "block";
	};

	var ugasniCakanje = function() {
		document.querySelector(".loading").style.display = "none";
	};
	
	document.querySelector("#upload").addEventListener("click", prizgiCakanje);

	//Pridobi seznam datotek
	var pridobiSeznamDatotek = function(event) {
		prizgiCakanje();
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				var datoteke = JSON.parse(xhttp.responseText);

				var datotekeHTML = document.querySelector("#datoteke");
		
				for (var i=0; i<datoteke.length; i++) {
					var datoteka = datoteke[i];

					var velikost = datoteka.Velikost;
					var enota = "B";
					if(velikost > 1024){
						velikost = Math.round(velikost/1024);
						enota = "KB";
					}
					if(velikost > 1024){
						velikost = Math.round(velikost/1024);
						enota = "MB";
					}
					if(velikost > 1024){
						velikost = Math.round(velikost/1024);
						enota = "GB";
					}

					datotekeHTML.innerHTML += " \
						<div class=' shadows'> \
							 <button type='button' id='file_modal' class='btn btn-success btn-block datoteka' data-toggle='modal' data-target='#file_data'>" + datoteka.Datoteka + "\xa0\xa0" +velikost + " " + enota + "</button> \
					    </div>";
				}
				
				if (datoteke.length > 0) {
					document.addEventListener('click', function(e){
						if(e.target.tagName=="BUTTON" && e.target.id=="file_modal"){
							var modal_out = document.querySelector(".modal-body");
							var output = e.target.innerText.split(/\s+/);
							var type = output[0].substring(output[0].lastIndexOf(".")+1, output[0].length);
							
							modal_out.innerHTML = " \
								<b><p> Ime datoteke: " + output[0] + "</p></b> \
								<b><p> Tip: " + type + "</p></b> \
								<b><p> Velikost: " + output[1] + " " + output[2] + "</p></b> \
								<div class='akcije'> \
								<span><a href='/preview/" + output[0] + "' target='_blank' class='btn btn-success btn-block datoteka shadows'>Predogled</a></span> \
								<span><a href='/download/" + output[0] + "' target='_self' class='btn btn-success btn-block datoteka shadows'>Shrani</a></span> \
								<span akcija='delete' datoteka='"+ output[0] + "' class='delete btn btn-success btn-block datoteka shadows'>Izbriši</span> </div> \
					    		</div>" 
					
							document.querySelector("span[akcija=delete]").addEventListener("click", brisi);
						}
					});
				}
				
				ugasniCakanje();
			}
		};
		xhttp.open("GET", "/datoteke", true);
		xhttp.send();
	};
	
	pridobiSeznamDatotek();

	var brisi = function(event) {
		prizgiCakanje();
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {

			if (xhttp.readyState == 4 && xhttp.status == 200) {
				if (xhttp.responseText == "Datoteka izbrisana") {
					window.location = "/";
				}
			}
			ugasniCakanje();
			location.reload();
		};
		xhttp.open("GET", "/delete/" + this.getAttribute("datoteka"), true);
		xhttp.send();
	};
});
