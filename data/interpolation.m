function interpolation(V,len)
  %funkcija interpolation(V,len) nariše interpolirano ploskev matrike V
  %najprej pridobimo velikost matrike V
  v1=size(V,1);
  v2=size(V,2);
  %dvojna for zanka za sprehod v x in y smeri
  for i = 1:1:v1-1
    for j = 1:1:v2-1
	  %priprava matrike data za funkcijo Z = interpolationFunction(data,len);
	  %prvi elementi vrstic predstavljajo dejanske vrednosti matrike V
    data(1:4,1) = [V(i,j);V(i,j+1);V(i+1,j);V(i+1,j+1)];
	  %drugi elementi vrstic so odvodi v smeri x
    data(1:4,2) = [odvX(V,i,j,v2);odvX(V,i,j+1,v2);odvX(V,i+1,j,v2);odvX(V,i+1,j+1,v2)];
	  %tretji elementi vrstic so odvodi v smeri y
    data(1:4,3) = [odvY(V,i,j,v1);odvY(V,i,j+1,v1);odvY(V,i+1,j,v1);odvY(V,i+1,j+1,v1)];
    Z = interpolationFunction(data,len);
    %for zanka za prepis elementov v glavno matriko A, ki jo na koncu narišemo
	  for k=1:1:len
        for l=1:1:len
          A((i-1)*(len-1)+k,(j-1)*(len-1)+l)=Z(k,l);
        endfor
       endfor
    endfor
  endfor
  %pripravimo si vrednosti, kjer bomo funkcijo narisali
  k=1/(len-1);
  x=1:k:size(V,2);
  y=1:k:size(V,1);
  [X,Y]=meshgrid(x,y);
  %izris matrike A in V (za potrebe funkcije demo in preverjanje pravilnosti
  %delovanja)
  figure(1);
  surf(X,Y,A);
  figure(2);
  surf(V)
endfunction
 
function dy = odvY(V,i,j,skrajnaVrednostY)
  %funkcija  dy = odvY(V,i,j,skrajnaVrednostY) vrne vrednos odvoda Y v tocki (i,j)
  if(i==1) %test skrajne vrednosti, kjer ne moremo dobiti predhodne
    dy = (V(i+1,j) - V(i,j));
  elseif(i==skrajnaVrednostY)%test skrajne vrednosti, kjer ne moremo dobiti naslednje
    dy = (V(i,j) - V(i-1,j));
  else %sicer lahko dobimo predhodno in naslednjo vrednost
    dy = 1/2 * (V(i+1,j) - V(i-1,j));
  endif
endfunction
 
function dx = odvX(V,i,j,skrajnaVrednostX)
   %funkcija  dx = odvX(V,i,j,skrajnaVrednostY) vrne vrednost odvoda X v tocki (i,j)
   if(j==1) %test skrajne vrednosti, kjer ne moremo dobiti predhodne
     dx = (V(i,j+1) - V(i,j));
   elseif(j==skrajnaVrednostX)%test skrajne vrednosti, kjer ne moremo dobiti naslednje
     dx = (V(i,j) - V(i,j-1));
   else %sicer lahko dobimo predhodno in naslednjo vrednost
     dx = 1/2 * (V(i,j+1) - V(i,j-1));
   endif
endfunction

%demonstracije funkcije interpolation(V,len)
%naredi matriko nakljucnih vrednosti velikosti 5x5
%te vrednosti nato interpolira in izri�e oba grafa (graf podatkov in
%interpolirane ploskve)
%na ta nacin sem preverjal delovanje funkcije interpolation
%!demo
%! A=rand(5,5);
%! interpolation(A,10);