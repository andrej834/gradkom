function Z = interpolationFunction(data, len)
  %Funkcija Z = interpolationFunction(data,len) vrne matriko Z
  %dimenzij len x len, ki predstavlja interpolirane vrednosti
  % enotskega kvadrata
  %najprej si pripravimo matriko velikosti len x len
  Z=zeros(len,len);
  %pripravimo si se vmesne vrednosti za interpolacijo enotskega kavdrata
  x=0:1/(len-1):1;
  y=0:1/(len-1):1;
  %matriki X,Y, predstavljata kombinacije vmesnih vrednosti enotskega kvadrata
  [X,Y]=meshgrid(x,y);
  %dvojna for zanka za dolocanje vrednosti matrike na indeksih (i,j)
  for i=1:1:len
    for j=1:1:len
      %vrednost matrike na mestu (i,j) je enaka vrednosti funkcije f(x,y)
      %printf("X: %d Y: %d\n",X(i,j),Y(i,j));
      Z(i,j)=getf(X(i,j),Y(i,j),data);
    endfor
  endfor
endfunction

function pol = p(x)
 %funkcija pol=p(x) vrne vrednost polinoma p(x) iz prve naloge  
 pol = 2*x^3-3*x^2+1;
endfunction
 
function W = getW(x, y)
  %funkcija W=getW(x,y) vrne ute�i wa wb wc wd
  W = zeros(4,1);
  W(1,1) = p(x)*p(y);
  W(2,1) = (1-p(x))*p(y);
  W(3,1) = p(x)*(1-p(y));
  W(4,1) = (1-p(x))*(1-p(y));
endfunction
 
function fa = getfa(x,y,vek)
  %funkcija fa = getfa(x,y,vek) vrne vrednost pomozne funkcije fa
  %vektor vek predstavlja vektor oblike [a,ax,ay]
  a=vek(1);
  ax=vek(2);
  ay=vek(3);
  fa=a+ax*x+ay*y;
endfunction
 
function fb = getfb(x,y,vek)
  %funkcija fb = getfb(x,y,vek) vrne vrednost pomozne funkcije fb 
  %vektor vek predstavlja vektor oblike [b,bx,by]
  b=vek(1);
  bx=vek(2);
  by=vek(3);
  fb=b+bx*(x-1)+by*y;
endfunction
 
function fc = getfc(x,y,vek)
  %funkcija fc = getfc(x,y,vek) vrne vrednost pomozne funkcije fc
  %vektor vek predstavlja vektor oblike [c,cx,cy]
  c=vek(1);
  cx=vek(2);
  cy=vek(3);
  fc=c+cx*x+cy*(y-1);
endfunction
 
function fd = getfd(x,y,vek)
  %funkcija fd = getfd(x,y,vek) vrne vrednost pomozne funkcije fd
  %vektor vek predstavlja vektor oblike [d,dx,dy]
   d=vek(1);
  dx=vek(2);
  dy=vek(3);
  fd=d+dx*(x-1)+dy*(y-1);
endfunction
 
function f = getf(x,y,data)
  %funkcija f = getf(x,y,data) vrne vrednost funkcije f na podlagi
  %vrednosti pomoznih funkcij fa, fb, fc, fd in utezi wa, wb, wc, wd
  fa=getfa(x,y,data(1,1:3));
  fb=getfb(x,y,data(2,1:3));
  fc=getfc(x,y,data(3,1:3));
  fd=getfd(x,y,data(4,1:3));
  W=getW(x,y);
  wa=W(1,1);
  wb=W(2,1);
  wc=W(3,1);
  wd=W(4,1);
  f=fa*wa+fb*wb+fc*wc+fd*wd;
endfunction

%Zelimo prevertiti delovanje funkcije interpolationFunction tako da preverimo
%kaj se zgodi v primeru, ko so podatki linearni oz. konstantni(vemo, da mora biti tudi rezultat
%funkcije f biti linearna oz. konstantna funkcija).
%demo za linearno funkijo
%!demo
%! figure(1);
%! k1=rand(1)*5;
%! k2=rand(1)*10;
%! n=rand(1)*2;
%! len=10;
%! [X,Y]=meshgrid(0:1/(len-1):1);
%! F=k1*X+k2*Y+n;
%! surf(X,Y,F);
%! figure(2);
%! s1=size(F,1);
%! s2=size(F,2);
%! data=[F(1,1) k1 k2; F(1,s2) k1 k2; F(s1, 1) k1 k2; F(s1,s2) k1 k2];
%! Z=interpolationFunction(data,len);
%! surf(X,Y,Z);

%test za linearno funkcijo
%!test
%! k1=rand(1)*5;
%! k2=rand(1)*10;
%! n=rand(1)*2;
%! len=10;
%! [X,Y]=meshgrid(0:1/(len-1):1);
%! F=k1*X+k2*Y+n;
%! s1=size(F,1);
%! s2=size(F,2);
%! data=[F(1,1) k1 k2; F(1,s2) k1 k2; F(s1, 1) k1 k2; F(s1,s2) k1 k2];
%! for i=1:1:10
%!  Z=interpolationFunction(data,len);
%!  assert(F,Z,0.05);
%! endfor

%test za konstantno funkcijo
%!test
%! len=10;
%! F=ones(len,len)*rand(1)*5;
%! s1=size(F,1);
%! s2=size(F,2);
%! data=[F(1,1) 0 0; F(1,s2) 0 0; F(s1, 1) 0 0; F(s1,s2) 0 0];
%! for i=1:1:10
%!  Z=interpolationFunction(data,len);
%!  assert(F,Z,0.05);
%! endfor

