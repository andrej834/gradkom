To get everything working you have to do the following:
 

``` sh
    npm install
    npm start
```
 
Then you can visit the page on **localhost:8080**.